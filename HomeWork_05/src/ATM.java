public class ATM {

    private int sumOfLeasMoney; /// 
    private int maxOutCache;
    private int  maxCapacity;
    private int operationsCount;


    public ATM(int sumOfLeasMoney, int maxOutCache, int maxCapacity) {
        if (sumOfLeasMoney >= maxCapacity) {
            throw new RuntimeException();
        }

        this.sumOfLeasMoney = sumOfLeasMoney;
        this.maxOutCache = maxOutCache;
        this.maxCapacity = maxCapacity;

    }

    public void setSumOfLeasMoney(int sumOfLeasMoney) {
        this.sumOfLeasMoney = sumOfLeasMoney;
    }

    public void setMaxOutCache(int maxOutCache) {
        this.maxOutCache = maxOutCache;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }


    public int getSumOfLeasMoney() {
        return sumOfLeasMoney;
    }

    public int getMaxOutCache() {
        return maxOutCache;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }


    public int putOnMoney(int incomeMoney) {
        if(getSumOfLeasMoney() + incomeMoney > getMaxCapacity()) {
            int valueToReturn = getSumOfLeasMoney() + incomeMoney - getMaxCapacity();
            setSumOfLeasMoney(getMaxCapacity());
            incrementOperations();
            return valueToReturn;
        }
        setSumOfLeasMoney(getSumOfLeasMoney() + incomeMoney);
        incrementOperations();
        return 0;
    }

    public int getMoney(int moneyGet) {
        if (getMaxOutCache() < moneyGet) {
            int valueToReturn = getSumOfLeasMoney() - getMaxCapacity();
            setSumOfLeasMoney(getSumOfLeasMoney() - getMaxCapacity());
            incrementOperations();
            return valueToReturn;
        }

        if (getSumOfLeasMoney() < moneyGet) {
            setSumOfLeasMoney(getSumOfLeasMoney() - moneyGet);
            incrementOperations();
            return moneyGet;
        }
        int valueToReturn = getSumOfLeasMoney();
        setSumOfLeasMoney(0);
        incrementOperations();
        return valueToReturn;

    }
    private void incrementOperations(){
        operationsCount++;
    }
}
