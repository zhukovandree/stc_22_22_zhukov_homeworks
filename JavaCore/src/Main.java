public class Main {
    public static void main(String[] args) {
        ProductsRepostory productsRepostory = new ProductsRepositoryFileBasedImpl("input.txt");

        Product milk = productsRepostory.findById(3);
        milk.setCount(25);
        milk.setPrice(70);

        productsRepostory.update(milk);
    }
}
